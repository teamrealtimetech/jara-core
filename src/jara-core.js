var jara = {
	elementByIndex : {},
	indexByElement : {},
	idByEvent : {},
	sendQueue : [],
	globalIndex : 0,
	globalEventId : 0,
	sessionId : -1,
	registerIndex : function(element) {
		var id = jara.nextIndex();
		jara.elementByIndex[id] = element;
		jara.indexByElement[element] = id;
	},
	nextEventId : function() {
		jara.globalEventId += 1;

		return jara.globalEventId;
	},
	nextIndex : function() {
		jara.globalIndex += 1;

		return jara.globalIndex;
	},
	init : function() {
		jara.registerElements(document, false);

		jara.send("/jara/ready" + document.location.search, {
			"path" : window.location.pathname,
			"source" : document.documentElement.innerHTML
		}, function(data) {
			jara.sessionId = data.session;
			jara.updateWithServer();
		}, function(data) {
			jara.updateWithServer();
		});

	},
	OPCODES : {
		ADD_NODE : "0",
		DELETE_NODE : "1",
		SET_ATTRIBUTE : "2",
		DELETE_ATTRIBUTE : "3",
		MODIFY_VALUE : "4",
		MODIFY_TEXT : "5",
		REGISTER_EVENT : "6",
		UNREGISTER_EVENT : "7",
		EXECUTE_JS: "8"
	},
	EVENT_TYPES : {
		EVENT_CLICK : "0",
		EVENT_KEY_UP : "1",
		EVENT_KEY_DOWN : "2",
		EVENT_VALUE_CHANGE : "3"
	},
	updateWithServer : function() {
		jara.send("/jara/update", {
			"session" : jara.sessionId
		}, function(data) {
			var rawCommand = decodeURIComponent(data.order);
			var commands = rawCommand.split(":");
			for (var index = 0; index < commands.length; index++) {
				var commandOpcode = commands[index];
				switch (commandOpcode) {
					case jara.OPCODES.ADD_NODE:
						var domSource = jara.decodeString(commands[++index]);

						var element = (new DOMParser().parseFromString(domSource, "text/html")).body.firstChild;

						var parentElement = jara.elementByIndex[commands[++index] * 1];
						var underElement = jara.elementByIndex[commands[++index] * 1];

						jara.registerElements(element, true);

						if (underElement != undefined) {
							parentElement.insertAfter(element, underElement);
						} else {
							parentElement.append(element);
						}

						break;
					case jara.OPCODES.DELETE_NODE:
						var targetElement = jara.elementByIndex[commands[++index] * 1];

						targetElement.remove();

						break;
					case jara.OPCODES.SET_ATTRIBUTE:
						var targetElement = jara.elementByIndex[commands[++index] * 1];

						var attributeName = jara.decodeString(commands[++index]);
						var attributeValue = jara.decodeString(commands[++index]);

						targetElement.setAttribute(attributeName, attributeValue);

						break;
					case jara.OPCODES.DELETE_ATTRIBUTE:
						var targetElement = jara.elementByIndex[commands[++index] * 1];

						var attributeName = jara.decodeString(commands[++index]);

						targetElement.removeAttribute(attributeName);

						break;
					case jara.OPCODES.MODIFY_VALUE:
						var targetElement = jara.elementByIndex[commands[++index] * 1];

						var value = jara.decodeString(commands[++index]);

						targetElement.value = value;

						break;
					case jara.OPCODES.MODIFY_TEXT:
						var targetElement = jara.elementByIndex[commands[++index] * 1];

						targetElement.textContent = jara.decodeString(commands[++index]);

						break;
					case jara.OPCODES.REGISTER_EVENT:
						var eventCode = jara.decodeString(commands[++index]);
						
						const finalIndex = commands[++index] * 1;
						const eventId = jara.nextEventId();
						
						var targetElement = jara.elementByIndex[finalIndex];
						
						var eventFunc = undefined;	
						var eventType = undefined;
						
						switch (eventCode) {
							case jara.EVENT_TYPES.EVENT_CLICK: {
								eventFunc = function() {
									jara.callEvent(finalIndex, eventId);
								};
								
								eventType = "click";
								break;
							}
							case jara.EVENT_TYPES.EVENT_VALUE_CHANGE: {
								eventFunc = function() {
									jara.callEvent(finalIndex, eventId, {
										"value" : this.value
									});
								};
								
								eventType = "change";
								break;
							}
						}
						
						jara.idByEvent[eventId] = eventFunc;
						targetElement.addEventListener(eventType, eventFunc);

						break;
					case jara.OPCODES.UNREGISTER_EVENT: 
						var eventCode = jara.decodeString(commands[++index]);
						var targetElement = jara.elementByIndex[commands[++index] * 1];
						var targetEventId = commands[++index] * 1;
						
						var eventType = undefined;
						
						switch (eventCode) {
							case jara.EVENT_TYPES.EVENT_CLICK: {
								eventType = "click";
								break;
							}
							case jara.EVENT_TYPES.EVENT_VALUE_CHANGE: {
								eventType = "change";
								break;
							}
						}
						
						targetElement.removeEventListener(eventType, jara.idByEvent[targetEventId]);

						break;
					case jara.OPCODES.EXECUTE_JS:
						var script = jara.decodeString(commands[++index]);
						eval(script);
						break;
				}
			}
			jara.updateWithServer();
		});
	},
	callEvent : function(index, eventId, arguments) {
		jara.send("/jara/event", Object.assign({
			"session" : jara.sessionId,
			"index" : index,
			"eventId" : eventId,
		}, arguments), function(data) {

		});
	},
	decodeString : function(str) {
		return str.split("&colon;").join(":");
	},
	registerElements : function(parentElement, needRegistery) {
		var children = parentElement.childNodes;

		if (parentElement.getAttribute != undefined) {
			if (parentElement.getAttribute("j:component") != null || parentElement.getAttribute("j:id") != null) {
				needRegistery = true;
			}
		}

		if (needRegistery) {
			jara.registerIndex(parentElement);
		}

		for (var i = 0; i < children.length; i++) {
			jara.registerElements(children[i], needRegistery);
		}
	},
	send : function(url, data, callback, errorCallback) {
		if (errorCallback == undefined) {
			errorCallback = function() {}
		}

		var xhr = new XMLHttpRequest();
		var urlEncodedData = "";
		var urlEncodedDataPairs = [];
		var name;

		for (name in data) {
			urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
		}

		urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
		xhr.onload = function() {
			if (xhr.status >= 200 && xhr.status < 300) {
				callback(jara.queryStringToJSON(xhr.responseText));
			} else {
				errorCallback();
			}
		};
		xhr.onreadystatechange = function() {
			if (xhr.status == 404) {
				errorCallback();
			}
		};

		xhr.open('POST', url);

		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		xhr.send(urlEncodedData);
	},
	queryStringToJSON : function(str) {
		var pairs = str.split('&');
		var result = {};
		pairs.forEach(function(pair) {
			pair = pair.split('=');
			var name = pair[0]
			var value = pair[1]
			if (name.length)
				if (result[name] !== undefined) {
					if (!result[name].push) {
						result[name] = [result[name]
						];
					}
					result[name].push(value || '');
				} else {
					result[name] = value || '';
				}
		});
		return (result);
	}
};
document.addEventListener("DOMContentLoaded", function(event) {
	jara.init();
});
