package com.realtimetech.jara.core.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.realtimetech.jara.core.Jara;
import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.application.component.builder.ComponentBuilder;
import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.dom.node.impl.Element;
import com.realtimetech.jara.core.route.Routable;
import com.realtimetech.jara.core.session.SessionFactory;

public abstract class Application {
	private static class StringLengthComparator implements Comparator<String> {
		public int compare(String o1, String o2) {
			return Integer.compare(o2.length(), o1.length());
		}
	}

	private Jara jara;
	private HashMap<String, ComponentBuilder<?>> componentBuilders;
	private HashMap<String, Routable> routes;
	private List<String> routePaths;

	private SessionFactory sessionFactory;

	public Application() {
		this.componentBuilders = new HashMap<String, ComponentBuilder<?>>();
		this.routes = new HashMap<String, Routable>();
		this.sessionFactory = new SessionFactory(this);
	}

	public void bind(Jara jara) {
		this.jara = jara;
	}

	public Jara getJara() {
		return jara;
	}

	public HashMap<String, Routable> getRoutes() {
		return routes;
	}

	public List<String> getRoutePaths() {
		return routePaths;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public abstract void onInitialize();

	public abstract void onReady();

	public abstract String getName();

	public Application registerRoute(String url, Routable routable) {
		this.routes.put(url, routable);
		this.routePaths = new ArrayList<String>(routes.keySet());

		Collections.sort(this.routePaths, new StringLengthComparator());

		return this;
	}

	public Application registerComponent(ComponentBuilder<?> componentBuilder) {
		this.componentBuilders.put(componentBuilder.getType(), componentBuilder);

		return this;
	}

	public Component buildComponent(Page page, Element element) {
		if (this.componentBuilders.containsKey(element.getComponentType())) {
			Component component = this.componentBuilders.get(element.getComponentType()).build(page);

			component.setElement(element);
			element.setComponent(component);

			return component;
		}

		return null;
	}

	public Routable getRouterByUrl(String url) {
		for (String routePath : routePaths) {
			if (url.startsWith(routePath)) {
				return routes.get(routePath);
			}
		}

		return null;
	}

	public String getUrlByRouter(Routable router) {
		for (String routePath : routes.keySet()) {
			if (routes.get(routePath) == router) {
				return routePath;
			}
		}

		return null;
	}
}