package com.realtimetech.jara.core.application.page.builder;

import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.session.Session;

public interface PageBuilder<T extends Page> {
	public abstract String getFilePath();
	
	public abstract T build(Session session);
}
