package com.realtimetech.jara.core.application.page;

import java.util.HashMap;

import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.application.component.ComponentTree;
import com.realtimetech.jara.core.application.component.observer.Updatable;
import com.realtimetech.jara.core.dom.Document;
import com.realtimetech.jara.core.dom.node.impl.Element;
import com.realtimetech.jara.core.session.Session;

public abstract class Page extends ComponentTree {
	private Session session;

	private Document virtualDocument;

	public Page(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

	public Element getElementByJId(String jId) {
		return this.getVirtualDocument().getElementByJId(jId);
	}

	public Component getComponentByJId(String jId) {
		return this.getVirtualDocument().getElementByJId(jId).getComponent();
	}

	public Document getVirtualDocument() {
		return virtualDocument;
	}

	public void setVirtualDocument(Document virtualDocument) {
		this.virtualDocument = virtualDocument;
	}

	@Override
	public void onUpdate(Updatable updatable) {
		session.update();
	}
	
	public abstract void onInitialize(Session session, HashMap<String, String> parameter);

	public abstract void onReady(Session session);
}