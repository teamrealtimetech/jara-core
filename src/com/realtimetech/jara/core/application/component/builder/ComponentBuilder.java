package com.realtimetech.jara.core.application.component.builder;

import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.application.page.Page;

public interface ComponentBuilder<T extends Component> {
	public abstract String getType();
	
	public abstract T build(Page page);
}
