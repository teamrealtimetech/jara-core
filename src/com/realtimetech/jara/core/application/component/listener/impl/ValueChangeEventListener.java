package com.realtimetech.jara.core.application.component.listener.impl;

import java.util.HashMap;

import com.realtimetech.jara.core.application.component.listener.EventListener;
import com.realtimetech.jara.core.application.component.listener.EventType;

public abstract class ValueChangeEventListener extends EventListener {
	@Override
	public EventType getEventType() {
		return EventType.EVENT_VALUE_CHANGE;
	}

	@Override
	public void onReceive(HashMap<String, String> arguments) {
		onChange(arguments.get("value"));
	}

	public abstract void onChange(String value);
}
