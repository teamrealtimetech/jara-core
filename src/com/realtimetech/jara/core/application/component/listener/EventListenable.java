package com.realtimetech.jara.core.application.component.listener;

import java.util.HashMap;

public interface EventListenable {
	public abstract EventType getEventType();

	public abstract void onReceive(HashMap<String, String> arguments);
}
