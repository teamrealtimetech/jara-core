package com.realtimetech.jara.core.application.component.listener;

public enum EventType {
	EVENT_CLICK("0"),
	EVENT_KEY_UP("1"),
	EVENT_KEY_DOWN("2"),
	EVENT_VALUE_CHANGE("3");

	private String eventCode;

	private EventType(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getEventCode() {
		return eventCode;
	}

	public static EventType tryParse(String eventCode) {
		for (EventType eventType : EventType.values()) {
			if (eventType.getEventCode().equalsIgnoreCase(eventCode)) {
				return eventType;
			}
		}

		return null;
	}
}
