package com.realtimetech.jara.core.application.component.listener;

import com.realtimetech.jara.core.dom.node.impl.Element;

public abstract class EventListener implements EventListenable {
	private long id;

	private Element element;
	
	public long getId() {
		return id;
	}
	
	public Element getElement() {
		return element;
	}
	
	public void bind(Element element) {
		this.element = element;
		this.id = element.getDocument().nextEventId();
	}
}
