package com.realtimetech.jara.core.application.component.listener.impl;

import java.util.HashMap;

import com.realtimetech.jara.core.application.component.listener.EventListener;
import com.realtimetech.jara.core.application.component.listener.EventType;

public abstract class ClickEventListener extends EventListener {
	@Override
	public EventType getEventType() {
		return EventType.EVENT_CLICK;
	}

	@Override
	public void onReceive(HashMap<String, String> arguments) {
		onClick();
	}

	public abstract void onClick();
}
