package com.realtimetech.jara.core.application.component;

import java.util.LinkedList;
import java.util.List;

import com.realtimetech.jara.core.application.component.observer.Updatable;
import com.realtimetech.jara.core.application.component.observer.UpdateObserver;

public abstract class ComponentTree extends Updatable implements UpdateObserver {
	private List<Component> components;

	public ComponentTree() {
		this.components = new LinkedList<Component>();
	}

	public ComponentTree removeComponent(Component component) {
		component.setParentTree(null);
		this.components.remove(component);

		return this;
	}

	public ComponentTree clearComponents() {
		for (Component component : new LinkedList<Component>(components)) {
			removeComponent(component);
		}
		
		return this;
	}

	public ComponentTree addComponent(Component component) {
		component.setParentTree(this);
		component.registerObserver(this);
		this.components.add(component);

		return this;
	}

	public List<Component> getComponents() {
		return this.components;
	}
}
