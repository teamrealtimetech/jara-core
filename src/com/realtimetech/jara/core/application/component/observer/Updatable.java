package com.realtimetech.jara.core.application.component.observer;

import java.util.LinkedList;
import java.util.List;

public abstract class Updatable {
	private List<UpdateObserver> updaterObservers;

	public Updatable() {
		this.updaterObservers = new LinkedList<UpdateObserver>();
	}

	public void registerObserver(UpdateObserver observer) {
		if (!updaterObservers.contains(observer)) {
			updaterObservers.add(observer);
		}
	}

	public void clearObservers() {
		for (UpdateObserver observer : updaterObservers) {
			observer.onUpdate(this);
		}
	}

	public void unregisterObserver(UpdateObserver observer) {
		if (updaterObservers.contains(observer)) {
			updaterObservers.remove(observer);
		}
	}

	public boolean update() {
		for (UpdateObserver observer : updaterObservers) {
			observer.onUpdate(this);
		}

		return true;
	}
}
