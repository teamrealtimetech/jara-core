package com.realtimetech.jara.core.application.component.observer;

public interface UpdateObserver {
	public void onUpdate(Updatable updatable);
}
