package com.realtimetech.jara.core.application.component;

import com.realtimetech.jara.core.application.component.observer.Updatable;
import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.dom.node.impl.Element;
import com.realtimetech.jara.core.dom.node.impl.RenderNode;
import com.realtimetech.jara.core.session.Session;

public abstract class Component extends ComponentTree {
	private ComponentTree parentTree;

	private Page page;
	private Element element;

	public Component(Page page, Updatable... updatables) {
		this.page = page;
		
		for(Updatable updatable : updatables) {
			updatable.registerObserver(this);
		}
	}

	public ComponentTree getParentTree() {
		return parentTree;
	}

	public void setParentTree(ComponentTree parentTree) {
		this.parentTree = parentTree;
	}

	public Page getPage() {
		return page;
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	public abstract void onInitialize(Session session);

	public abstract boolean onRender(RenderNode renderNode);

	@Override
	public void onUpdate(Updatable updatable) {
		render();
	
		update();
	}

	public void reset() {
		this.element.clearChildNodes();
		this.clearComponents();
		
		this.onInitialize(page.getSession());
		
		this.onUpdate(this);
	}
	
	private boolean render() {
		RenderNode renderNode = new RenderNode(element);

		if (this.onRender(renderNode)) {
			renderNode.updateTreeDown();
			element.reflectionTree(renderNode);
			
			for (Component component : getComponents()) {
				if (component.getElement().isLinked()) {
					component.render();
				}
			}
		
			return true;
		}
		
		return false;
	}
}