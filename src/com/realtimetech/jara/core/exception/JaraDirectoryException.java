package com.realtimetech.jara.core.exception;

import com.realtimetech.jara.core.Jara;

public class JaraDirectoryException extends Exception {
	private static final long serialVersionUID = -3207421086757577004L;

	private Jara jara;

	public JaraDirectoryException(Jara jara) {
		this.jara = jara;
	}

	public Jara getJara() {
		return jara;
	}

	@Override
	public String getMessage() {
		return "Not found application directory";
	}
}
