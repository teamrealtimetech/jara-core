package com.realtimetech.jara.core.route;

import java.io.File;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.ByteContent;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

public abstract class StaticRoute implements Routable {
	private Application application;

	public StaticRoute(Application application) {
		this.application = application;
	}

	public Application getApplication() {
		return application;
	}

	private String getFileExtension(File file) {
		String name = file.getName();
		int lastIndexOf = name.lastIndexOf(".");
		if (lastIndexOf == -1) {
			return "";
		}
		return name.substring(lastIndexOf);
	}

	@Override
	public boolean route(String path, HttpResponse httpResponse) {
		String pathname = getApplication().getJara().getRootDirectory().getAbsolutePath() + getStaticPath(path, httpResponse);
		File file = new File(pathname);

		if (file.exists()) {
			ByteContent content = new ByteContent();

			content.setMimeType(MimeType.tryParseFileExtension(getFileExtension(file)));
			content.setBytes(httpResponse.getStream().getConnection().getHttpServer().getFileStorage().getFileBytes(httpResponse.getHttpRequest(), file));

			httpResponse.setContent(content);
			httpResponse.setResponseCode(200);

			return true;
		} else {
			httpResponse.setResponseCode(404);

			return false;
		}
	}

	public abstract String getStaticPath(String path, HttpResponse httpResponse);
}
