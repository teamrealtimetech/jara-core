package com.realtimetech.jara.core.route;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.application.page.builder.PageBuilder;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.ByteContent;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

public class PageRoute implements Routable {
	private final byte[] NEW_LINE_BYTES = "\r\n".getBytes();
	private final byte[] OPEN_SCRIPT_TAG = "<script>".getBytes();
	private final byte[] CLOSE_SCRIPT_TAG = "</script>".getBytes();
	private static byte[] CORE_JS_BYTES;
	{
		try {
			CORE_JS_BYTES = getClass().getResourceAsStream("/jara-core.js").readAllBytes();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Application application;

	private ByteArrayOutputStream byteArrayOutputStream;

	private PageBuilder<?> pageBuilder;

	public PageRoute(Application application, PageBuilder<?> pageBuilder) {
		this.application = application;

		this.pageBuilder = pageBuilder;

		this.byteArrayOutputStream = new ByteArrayOutputStream();
	}

	public Application getApplication() {
		return application;
	}

	public PageBuilder<?> getPageBuilder() {
		return pageBuilder;
	}

	public synchronized byte[] mergeBytes(byte[] fileBytes) {
		try {
			this.byteArrayOutputStream.reset();
			this.byteArrayOutputStream.write(OPEN_SCRIPT_TAG);
			this.byteArrayOutputStream.write(NEW_LINE_BYTES);
			this.byteArrayOutputStream.write(CORE_JS_BYTES);
			this.byteArrayOutputStream.write(NEW_LINE_BYTES);
			this.byteArrayOutputStream.write(CLOSE_SCRIPT_TAG);
			this.byteArrayOutputStream.write(NEW_LINE_BYTES);
			this.byteArrayOutputStream.write(fileBytes);
		} catch (IOException e) {
		}

		return this.byteArrayOutputStream.toByteArray();
	}

	@Override
	public boolean route(String path, HttpResponse httpResponse) {
		String pathname = getApplication().getJara().getRootDirectory().getAbsolutePath() + this.pageBuilder.getFilePath();
		File file = new File(pathname);

		if (file.exists()) {
			byte[] fileBytes = httpResponse.getStream().getConnection().getHttpServer().getFileStorage().getFileBytes(httpResponse.getHttpRequest(), file);

			ByteContent content = new ByteContent();

			content.setMimeType(MimeType.TEXT_HTML);
			content.setBytes(mergeBytes(fileBytes));

			httpResponse.setContent(content);
			httpResponse.setResponseCode(200);

			return true;
		} else {
			httpResponse.setResponseCode(404);
		}

		return false;
	}
}
