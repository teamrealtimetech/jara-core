package com.realtimetech.jara.core.route;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

public class FileRoute extends StaticRoute {
	private String filePath;

	public FileRoute(Application application, String filePath) {
		super(application);

		this.filePath = filePath;
	}

	public String getFilePath() {
		return filePath;
	}

	@Override
	public String getStaticPath(String path, HttpResponse httpResponse) {
		return filePath;
	}
}
