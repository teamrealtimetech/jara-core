package com.realtimetech.jara.core.route;

import com.realtimetech.jara.http.network.message.response.HttpResponse;

public interface Routable {
	public boolean route(String path, HttpResponse httpResponse);
}
