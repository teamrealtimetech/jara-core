package com.realtimetech.jara.core.route;

import java.io.File;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

public class DirectoryRoute extends StaticRoute {
	private String directory;

	public DirectoryRoute(Application application, String directory) {
		super(application);

		this.directory = directory;
	}

	public String getDirectory() {
		return directory;
	}

	@Override
	public String getStaticPath(String path, HttpResponse httpResponse) {
		if (path.equals("")) {
			path = "index.html";
		}

		return directory + File.separator + path;
	}
}
