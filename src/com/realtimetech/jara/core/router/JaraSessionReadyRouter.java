package com.realtimetech.jara.core.router;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.route.PageRoute;
import com.realtimetech.jara.core.route.Routable;
import com.realtimetech.jara.core.session.Session;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.FormContent;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.Priority;
import com.realtimetech.jara.http.route.RequestRouter;

public class JaraSessionReadyRouter extends RequestRouter {
	private Application application;

	public JaraSessionReadyRouter(Priority priority, Application application) {
		super(priority);
		this.application = application;
	}

	@Override
	public String requestUrl() {
		return "/jara/ready";
	}

	@Override
	public MethodType requestMethod() {
		return MethodType.POST;
	}

	@Override
	public MimeType requestMimeType() {
		return MimeType.APPLICATION_X_WWW_FORM_URLENCODED;
	}

	@Override
	public boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse) {
		if (httpRequest.getContent() instanceof FormContent) {
			FormContent formContent = (FormContent) httpRequest.getContent();

			String path = formContent.getContents().get("path");
			String source = formContent.getContents().get("source");

			Routable routerByUrl = application.getRouterByUrl(path);

			if (routerByUrl != null && routerByUrl instanceof PageRoute) {
				PageRoute pageRoute = (PageRoute) routerByUrl;

				Session createSession = application.getSessionFactory().createSession();

				createSession.ready(httpRequest, pageRoute.getPageBuilder().build(createSession), "<html>" + source + "</html>");

				FormContent content = new FormContent();

				content.getContents().put("session", createSession.getId() + "");
				httpResponse.setResponseCode(200);
				httpResponse.setContent(content);

				for (Component component : createSession.getPage().getComponents()) {
					component.onInitialize(createSession);
					
					component.onUpdate(createSession.getPage());
				}
				
				createSession.getPage().onReady(createSession);
				
				createSession.update();
				return true;
			}

		}
		httpResponse.setResponseCode(500);

		return true;
	}

	public Application getApplication() {
		return application;
	}
}
