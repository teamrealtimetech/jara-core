package com.realtimetech.jara.core.router;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.route.Routable;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.Priority;
import com.realtimetech.jara.http.route.RequestRouter;

public class JaraRequestRouter extends RequestRouter {
	private Application application;

	public JaraRequestRouter(Priority priority, Application application) {
		super(priority);

		this.application = application;
	}

	public Application getApplication() {
		return application;
	}

	@Override
	public String requestUrl() {
		return "/*";
	}

	@Override
	public MethodType requestMethod() {
		return MethodType.GET;
	}

	@Override
	public MimeType requestMimeType() {
		return MimeType.ALL;
	}

	@Override
	public boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse) {
		String path = httpRequest.getPath();
		Routable router = application.getRouterByUrl(path);

		if (router != null) {
			String routePath = application.getUrlByRouter(router);
			return router.route(path.substring(routePath.length(), path.length()), httpResponse);
		}

		return false;
	}
}
