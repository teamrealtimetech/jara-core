package com.realtimetech.jara.core.router;

import java.util.List;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.dom.node.Node;
import com.realtimetech.jara.core.session.Session;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.FormContent;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.Priority;
import com.realtimetech.jara.http.route.RequestRouter;

public class JaraUpdateRouter extends RequestRouter {
	private Application application;

	public JaraUpdateRouter(Priority priority, Application application) {
		super(priority);
		this.application = application;
	}

	@Override
	public String requestUrl() {
		return "/jara/update";
	}

	@Override
	public MethodType requestMethod() {
		return MethodType.POST;
	}

	@Override
	public MimeType requestMimeType() {
		return MimeType.APPLICATION_X_WWW_FORM_URLENCODED;
	}

	public static void debugAllTree(Node node, String prefix) {
		List<Node> nodes = node.getChildNodes();
		System.out.println(prefix + node.getHash() + " " + node + "\t" + node.getIndex());
		for (int index = 0; index < nodes.size(); index++) {
			debugAllTree(nodes.get(index), prefix + "  ");
		}
	}

	@Override
	public boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse) {
		if (httpRequest.getContent() instanceof FormContent) {
			FormContent formContent = (FormContent) httpRequest.getContent();

			String session = formContent.getContents().get("session");

			try {
				Long sessionId = Long.parseLong(session);

				Session sessionById = getApplication().getSessionFactory().getSessionById(sessionId);
				if (sessionById != null) {
					sessionById.setHttpUpdateResponse(httpResponse);
					httpResponse.pending();
					
					if (sessionById.getPage().getVirtualDocument().getChanges().getCount() != 0) {
						sessionById.update();
					}
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		httpResponse.setResponseCode(500);

		return true;
	}

	public Application getApplication() {
		return application;
	}
}
