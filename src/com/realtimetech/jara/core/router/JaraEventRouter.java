package com.realtimetech.jara.core.router;

import java.util.HashMap;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.dom.node.Node;
import com.realtimetech.jara.core.dom.node.impl.Element;
import com.realtimetech.jara.core.session.Session;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.FormContent;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.Priority;
import com.realtimetech.jara.http.route.RequestRouter;

public class JaraEventRouter extends RequestRouter {
	private Application application;

	public JaraEventRouter(Priority priority, Application application) {
		super(priority);
		this.application = application;
	}

	@Override
	public String requestUrl() {
		return "/jara/event";
	}

	@Override
	public MethodType requestMethod() {
		return MethodType.POST;
	}

	@Override
	public MimeType requestMimeType() {
		return MimeType.APPLICATION_X_WWW_FORM_URLENCODED;
	}

	@Override
	public boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse) {
		if (httpRequest.getContent() instanceof FormContent) {
			FormContent formContent = (FormContent) httpRequest.getContent();

			String session = formContent.getContents().get("session");

			try {
				Long sessionId = Long.parseLong(session);

				Session sessionById = getApplication().getSessionFactory().getSessionById(sessionId);
				if (sessionById != null) {
					Long index = Long.parseLong(formContent.getContents().get("index"));
					Long eventId = Long.parseLong(formContent.getContents().get("eventId"));

					HashMap<String, String> arguments = new HashMap<String, String>(formContent.getContents());

					arguments.remove("index");
					arguments.remove("session");
					arguments.remove("eventCode");

					Node nodeByIndex = sessionById.getPage().getVirtualDocument().getNodeByIndex(index);

					if (nodeByIndex instanceof Element) {
						Element element = (Element) nodeByIndex;

						element.callEvent(eventId, arguments);
					}

					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		httpResponse.setResponseCode(500);

		return true;
	}

	public Application getApplication() {
		return application;
	}
}
