package com.realtimetech.jara.core;

import java.io.File;

import com.realtimetech.carbon.Carbon;
import com.realtimetech.carbon.Version;
import com.realtimetech.carbon.logger.Logger;
import com.realtimetech.carbon.logger.level.Level;
import com.realtimetech.carbon.properties.OptionProperties;
import com.realtimetech.carbon.server.Server;
import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.exception.JaraDirectoryException;
import com.realtimetech.jara.core.router.JaraEventRouter;
import com.realtimetech.jara.core.router.JaraRequestRouter;
import com.realtimetech.jara.core.router.JaraSessionReadyRouter;
import com.realtimetech.jara.core.router.JaraUpdateRouter;
import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.route.Priority;

public class Jara extends Server {
	private Logger logger = Carbon.getInstance().getLogger(Jara.class);
	private HttpServer httpServer;

	private Application application;

	private File rootDirectory;

	Jara() throws JaraDirectoryException {
	}

	public Jara(String rootDirectory) throws JaraDirectoryException {
		this.rootDirectory = new File(rootDirectory);

		this.httpServer = (HttpServer) Server.createServer(HttpServer.class);
	}

	public Jara(String rootDirectory, int port, String keyStoreFileName, String keyStorePassword) throws JaraDirectoryException {
		this.rootDirectory = new File(rootDirectory);

		this.httpServer = new HttpServer(port, keyStoreFileName, keyStorePassword);
	}

	public Application getApplication() {
		return application;
	}

	public Jara bindApplication(Application application) {
		this.application = application;
		this.application.bind(this);
		return this;
	}

	public File getRootDirectory() {
		return rootDirectory;
	}

	@Override
	public String getName() {
		return "Jara";
	}

	public Version createVersion() {
		return new Version(0, 0, 5);
	}

	@Override
	protected String getPropertiesFileName() {
		return "server.properties";
	}

	@Override
	protected boolean onInitialize() {
		this.httpServer.registerRouter(new JaraEventRouter(Priority.SUPER, application));
		this.httpServer.registerRouter(new JaraSessionReadyRouter(Priority.SUPER, application));
		this.httpServer.registerRouter(new JaraUpdateRouter(Priority.SUPER, application));
		this.httpServer.registerRouter(new JaraRequestRouter(Priority.SUPER, application));

		if (this.rootDirectory.exists() && this.rootDirectory.isDirectory()) {
			if (this.application == null) {
				logger.log(Level.ERROR, "Can't initialize because application is not bind.");

				return false;
			}

			this.application.onInitialize();

			return true;
		}

		logger.log(Level.ERROR, "Can't initialize because root directory is not exist.");
		return false;
	}

	@Override
	protected boolean onStart() {
		this.httpServer.start();

		return true;
	}

	@Override
	protected boolean onStop() {
		this.httpServer.stop();

		return true;
	}

	@Override
	protected Server newServerInstance(OptionProperties optionProperties) {
		return null;
	}
}
