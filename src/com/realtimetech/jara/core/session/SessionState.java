package com.realtimetech.jara.core.session;

public enum SessionState {
	NOT_READY, RUNNING, CLOSE;
}
