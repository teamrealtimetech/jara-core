package com.realtimetech.jara.core.session;

import java.io.IOException;

import org.xml.sax.SAXException;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.dom.changes.Changes;
import com.realtimetech.jara.core.dom.io.DocumentReader;
import com.realtimetech.jara.http.network.message.FormContent;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

public class Session {
	private long id;
	
	private Application application;

	private HttpResponse httpUpdateResponse;
	private SessionState state;
	private Page page;

	private DocumentReader documentReader;

	public Session(Application application, long id) {
		this.application = application;
		this.id = id;
		this.httpUpdateResponse = null;
		this.state = SessionState.NOT_READY;

		this.documentReader = new DocumentReader(application);
	}
	
	public Application getApplication() {
		return application;
	}

	public long getId() {
		return id;
	}

	public SessionState getState() {
		return state;
	}

	public Page getPage() {
		return page;
	}

	public void setHttpUpdateResponse(HttpResponse httpUpdateResponse) {
		this.httpUpdateResponse = httpUpdateResponse;
	}

	public boolean update() {
		Changes changes = page.getVirtualDocument().getChanges();

		synchronized (changes) {
			if (httpUpdateResponse != null && changes.getCount() != 0) {
				FormContent sendContent = new FormContent();
				sendContent.getContents().put("order", changes.toString());

				changes.clear();
				httpUpdateResponse.setContent(sendContent);
				httpUpdateResponse.setResponseCode(200);

				httpUpdateResponse.response();
				httpUpdateResponse = null;
				return true;
			}
		}

		return false;
	}

	public void ready(HttpRequest httpRequest, Page page, String source) {
		try {
			this.page = page;
			this.page.onInitialize(this, httpRequest.getParameters());

			this.page.setVirtualDocument(documentReader.read(page, source));
			this.page.getVirtualDocument().active();

			this.state = SessionState.RUNNING;
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
