package com.realtimetech.jara.core.session;

import java.util.HashMap;

import com.realtimetech.jara.core.application.Application;

public class SessionFactory {
	private Application application;
	private HashMap<Long, Session> sessions;

	private long globalSessionId;

	public SessionFactory(Application application) {
		this.application = application;
		this.sessions = new HashMap<Long, Session>();
		this.globalSessionId = 0;
	}

	public Application getApplication() {
		return application;
	}

	public synchronized Session createSession() {
		globalSessionId = globalSessionId + 1;
		Session session = new Session(application, globalSessionId);

		this.sessions.put(globalSessionId, session);

		return session;
	}

	public Session getSessionById(long id) {
		return this.sessions.get(id);
	}
}
