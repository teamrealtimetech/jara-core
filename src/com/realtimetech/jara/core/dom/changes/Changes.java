package com.realtimetech.jara.core.dom.changes;

public class Changes {
	private StringBuilder domChanges;
	private int count;

	public Changes() {
		this.domChanges = new StringBuilder();
		this.count = 0;
	}

	public void clear() {
		this.domChanges.setLength(0);
		this.count = 0;
	}

	public void write(Changes changes) {
		if (changes.count != 0) {
			if (count != 0) {
				domChanges.append(":");
			}
			domChanges.append(changes.toString());
			count += changes.count;

			changes.clear();
		}
	}

	public void write(ChangeType changeType, Object... objects) {
		if (domChanges.length() != 0) {
			domChanges.append(":");
		}
		domChanges.append(changeType.getOperationCode());

		for (Object object : objects) {
			domChanges.append(":");
			domChanges.append(encode(object.toString()));
		}

		count++;
	}

	public int getCount() {
		return count;
	}

	public String encode(String value) {
		return value.replace(":", "&colon;");
	}

	public String decode(String value) {
		return value.replace("&colon;", ":");
	}

	@Override
	public String toString() {
		return domChanges.toString();
	}
}
