package com.realtimetech.jara.core.dom.changes;

public enum ChangeType {
	ADD_NODE("0"),
	DELETE_NODE("1"),
	SET_ATTRIBUTE("2"),
	DELETE_ATTRIBUTE("3"),
	MODIFY_VALUE("4"),
	MODIFY_TEXT("5"),
	REGISTER_EVENT("6"),
	UNREGISTER_EVENT("7"),
	EXECUTE_JS("8");
	
	private String operationCode;

	private ChangeType(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationCode() {
		return operationCode;
	}
}
