package com.realtimetech.jara.core.dom.node;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.realtimetech.jara.core.application.component.listener.EventListener;
import com.realtimetech.jara.core.dom.Document;
import com.realtimetech.jara.core.dom.changes.ChangeType;
import com.realtimetech.jara.core.dom.node.builder.ElementBuilder;
import com.realtimetech.jara.core.dom.node.impl.Element;

public abstract class Node {
	private LinkedList<Node> childNodes;

	private Document document;
	private Node parentNode;

	private long index;
	private boolean watch;

	private StringBuilder checksumBuilder;
	private String checksum;

	public Node() {
		this.childNodes = new LinkedList<Node>();

		this.index = -1;
		this.watch = false;

		this.checksumBuilder = new StringBuilder();

		this.checksum = null;
		this.document = null;
	}

	public boolean isWatch() {
		if (watch) {
			return true;
		}

		if (getParentNode() != null) {
			boolean status = false;

			status = getParentNode().isWatch();

			if (status) {
				this.watch = true;
			}
		}

		return watch;
	}

	public void setWatch(boolean watch) {
		this.watch = watch;
	}

	public Document getDocument() {
		return document;
	}

	public Node getParentNode() {
		return parentNode;
	}

	public LinkedList<Node> getChildNodes() {
		return childNodes;
	}

	public long getIndex() {
		return index;
	}

	public void setIndex(long jaraId) {
		this.index = jaraId;
	}

	public Node addChildNode(Node node) {
		return insertChildNode(node, childNodes.size());
	}

	public Node insertChildNode(Node node, int index) {
		node.parentNode = this;

		this.childNodes.add(index, node);

		if (node.isWatch()) {
			long beforeId = -1;

			if (index + 1 < childNodes.size()) {
				beforeId = this.childNodes.get(index + 1).getIndex();
			}

			this.getDocument().writeChange(ChangeType.ADD_NODE, node.getCode(), this.getIndex(), beforeId);
		}

		node.updateTreeDown();
		node.refreshEventListener();

		return this;
	}

	public Node removeChildNode(Node node) {
		node.parentNode = null;

		this.childNodes.remove(node);

		if (node.isWatch()) {
			this.getDocument().writeChange(ChangeType.DELETE_NODE, node.getIndex());
		}

		this.updateTreeUp();
		return this;
	}

	public Node clearChildNodes() {
		for (Node node : childNodes) {
			node.parentNode = null;
			removeChildNode(node);
		}
//		this.childNodes.clear();

		this.updateTreeUp();
		return this;
	}

	public void refreshEventListener() {
		for (Node node : childNodes) {
			node.refreshEventListener();
		}

		if (this instanceof Element) {
			Element element = (Element) this;

			for (EventListener listener : element.getListeners()) {
				listener.bind(element);

				this.getDocument().writeChange(ChangeType.REGISTER_EVENT, listener.getEventType().getEventCode(),
						getIndex());
			}
		}
	}

	public boolean reflection(Node newNode) {
		if (!isWatch())
			return false;

		int prevChangeCount = this.getDocument().getChanges().getCount();

		reflectionNode(newNode);
		if (this instanceof Element) {
			Element element = (Element) this;

			if (element.getComponent() != null) {
				return false;
			}
		}
		reflectionTree(newNode);

		return this.getDocument().getChanges().getCount() == prevChangeCount;
	}

	public void reflectionTree(Node newNode) {
		if (!this.getChecksum().equals(newNode.getChecksum())) {
			LinkedList<Node> addedNodes = new LinkedList<Node>(newNode.getChildNodes());
			LinkedList<Node> removedNodes = new LinkedList<Node>(getChildNodes());

			HashMap<Node, Node> updateMap = new HashMap<Node, Node>();

			HashMap<String, List<Node>> checksumNodes = new HashMap<String, List<Node>>();

			for (Node oldNode : new LinkedList<Node>(removedNodes)) {
				String checksum = oldNode.getChecksum();
				if (!checksumNodes.containsKey(checksum)) {
					checksumNodes.put(checksum, new LinkedList<>());
				}
				checksumNodes.get(checksum).add(oldNode);
			}

			for (Node currentNode : new LinkedList<Node>(addedNodes)) {
				String checksum = currentNode.getChecksum();

				if (checksumNodes.containsKey(checksum)) {
					List<Node> oldNodes = checksumNodes.get(checksum);

					if (oldNodes.size() > 0) {
						Node oldNode = oldNodes.remove(0);
						addedNodes.remove(currentNode);
						removedNodes.remove(oldNode);

						updateMap.put(oldNode, currentNode);
					}
				}
			}

			for (Node currentNode : new LinkedList<Node>(addedNodes)) {
				for (Node oldNode : new LinkedList<Node>(removedNodes)) {
					if (oldNode.getHash().equals(currentNode.getHash())) {
						addedNodes.remove(currentNode);
						removedNodes.remove(oldNode);

						updateMap.put(oldNode, currentNode);
						break;
					}
				}
			}

			for (Node addedNode : addedNodes) {
				int indexOf = addedNode.getParentNode().childNodes.indexOf(addedNode);

				insertChildNode(addedNode, indexOf);
			}

			for (Node removedNode : removedNodes) {
				removeChildNode(removedNode);
			}

			for (Node currentNode : updateMap.keySet()) {
				currentNode.reflection(updateMap.get(currentNode));
			}
		} else {
			LinkedList<Node> addedNodes = new LinkedList<Node>(newNode.getChildNodes());
			LinkedList<Node> removedNodes = new LinkedList<Node>(getChildNodes());

			for (int i = 0; i < addedNodes.size(); i++) {
				removedNodes.get(i).reflection(addedNodes.get(i));
			}
		}
	}

	public abstract void reflectionNode(Node node);

	public abstract String getCode();

	public abstract String getHash();

	public void updateTreeUp() {
		updateChecksum();

		if (this.parentNode != null) {
			this.parentNode.updateTreeUp();
		}
	}

	public void updateTreeDown() {
		if (this.parentNode != null) {
			this.document = this.parentNode.getDocument();
		}

		if (this.isWatch()) {
			this.getDocument().registerNode(this);
		}

		if (childNodes.size() == 0) {
			updateTreeUp();
		} else {
			for (Node node : childNodes) {
				node.parentNode = this;
				node.updateTreeDown();
			}
		}
	}

	private void updateChecksum() {
		checksumBuilder.setLength(0);

		checksumBuilder.append(" ");
		checksumBuilder.append(getHash());

		if (childNodes.size() != 0) {
			checksumBuilder.append(">");

			for (Node node : childNodes) {
				checksumBuilder.append(node.getChecksum());
			}
		}
		checksumBuilder.append("#");

		this.checksum = checksumBuilder.toString();
	}

	public String getChecksum() {
		if (checksum == null) {
			updateTreeUp();
		}

		return checksum;
	}

	public boolean isLinked() {
		return parentNode != null;
	}

	public Element $(String tagType) {
		return this.$(new Element(tagType), null);
	}

	public Element $(String tagType, ElementBuilder elementBuilder) {
		return this.$(new Element(tagType), elementBuilder);
	}

	public Element $(Element element) {
		return this.$(element, null);
	}

	public Element $(Element element, ElementBuilder elementBuilder) {
		this.addChildNode(element);

		if (elementBuilder != null) {
			elementBuilder.build(element);
		}

		return element;
	}
}
