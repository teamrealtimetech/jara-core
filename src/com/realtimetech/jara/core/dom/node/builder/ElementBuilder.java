package com.realtimetech.jara.core.dom.node.builder;

import com.realtimetech.jara.core.dom.node.impl.Element;

@FunctionalInterface
public interface ElementBuilder {
	public abstract void build(Element parent);
}
