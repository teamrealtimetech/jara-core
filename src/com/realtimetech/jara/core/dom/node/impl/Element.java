package com.realtimetech.jara.core.dom.node.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.application.component.listener.EventListener;
import com.realtimetech.jara.core.dom.changes.ChangeType;
import com.realtimetech.jara.core.dom.node.Node;

public class Element extends Node {
	public static class Elements {
		private LinkedList<Element> elements;

		public Elements() {
			this.elements = new LinkedList<Element>();
		}

		public Elements add(Element element) {
			this.elements.add(element);

			return this;
		}

		public Elements add(Elements elements) {
			for (Element element : elements.getList()) {
				this.elements.add(element);
			}

			return this;
		}

		public LinkedList<Element> getList() {
			return this.elements;
		}
	}

	private Component component;

	private HashMap<String, String> attributes;
	private StringBuilder stringBuilder;

	private String name;

	private String value;

	private List<EventListener> listeners;

	public Element(String name) {
		this.attributes = new HashMap<String, String>();
		this.listeners = new LinkedList<EventListener>();
		this.stringBuilder = new StringBuilder();
		this.name = name;
		this.value = "";
		this.component = null;
	}

	public List<EventListener> getListeners() {
		return listeners;
	}

	public void registerEventListener(EventListener eventListener) {
		eventListener.bind(this);
		this.listeners.add(eventListener);

		this.getDocument().writeChange(ChangeType.REGISTER_EVENT, eventListener.getEventType().getEventCode(), getIndex());
	}

	public void unregisterEventListener(EventListener eventListener) {
		if(this.listeners.contains(eventListener)) {
			this.listeners.remove(eventListener);

			this.getDocument().writeChange(ChangeType.UNREGISTER_EVENT, eventListener.getEventType().getEventCode(), getIndex(), eventListener.getId());
		}
	}

	public void callEvent(long eventId, HashMap<String, String> arguments) {
		for (EventListener eventListener : listeners) {
			if (eventListener.getId() == eventId) {
				eventListener.onReceive(arguments);
			}
		}
	}

	public String getValue() {
		return value;
	}

	public Element setValueWithoutChange(String value) {
		this.value = value;
		return this;
	}

	public Element setValue(String value) {
		this.value = value;
		this.getDocument().writeChange(ChangeType.MODIFY_VALUE, this.getIndex(), value);

		return this;
	}

	public String getName() {
		return name;
	}

	public HashMap<String, String> getAttributes() {
		return attributes;
	}

	public Element removeAttribute(String name) {
		this.attributes.remove(name.toLowerCase());
		this.getDocument().writeChange(ChangeType.DELETE_ATTRIBUTE, this.getIndex(), name);

		return this;
	}

	public Element setAttribute(String name, String value) {
		this.attributes.put(name.toLowerCase(), value);
		this.getDocument().writeChange(ChangeType.SET_ATTRIBUTE, this.getIndex(), name, value);

		return this;
	}

	public Element setText(String str) {
		this.getChildNodes().clear();
		this.addChildNode(new TextNode(str));

		return this;
	}

	public Element addClass(String className) {
		String classValue = getAttribute("class");

		if (classValue == null) {
			setAttribute("class", className);
		} else {
			setAttribute("class", classValue + " " + className);
		}

		return this;
	}

	public Element removeClass(String className) {
		String classValue = getAttribute("class");

		if (classValue != null) {
			if (className.equalsIgnoreCase(classValue)) {
				removeAttribute("class");
			} else {
				setAttribute("class", classValue.replace(" " + className, ""));
			}
		}

		return this;
	}

	public boolean containsClass(String className) {
		String classValue = getAttribute("class");

		if (classValue != null) {
			String[] splitClass = classValue.split(" ");

			for (String classData : splitClass) {
				if (classData.equalsIgnoreCase(className)) {
					return true;
				}
			}
		}

		return false;
	}

	public String getAttribute(String name) {
		return this.attributes.get(name);
	}

	public Set<String> getAttributeNames() {
		return this.attributes.keySet();
	}

	public Element getElementById(String id) {
		return getElementByAttribute("id", id);
	}

	public Component getComponent() {
		return component;
	}

	public Element getElementByAttribute(String key, String value) {
		Elements elements = this.getElementsByAttribute(new Elements(), key, value);

		LinkedList<Element> list = elements.getList();
		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public Elements getElementsByAttribute(String key, String value) {
		return this.getElementsByAttribute(new Elements(), key, value);
	}

	private Elements getElementsByAttribute(Elements elements, String key, String value) {
		String attribute = this.getAttribute(key);
		if (attribute != null && attribute.equals(value)) {
			elements.add(this);
		}

		for (Node node : this.getChildNodes()) {
			if (node instanceof Element) {
				Element tagNode = (Element) node;

				tagNode.getElementsByAttribute(elements, key, value);
			}
		}

		return elements;
	}

	@Override
	public void reflectionNode(Node node) {
		Element element = (Element) node;

		for (String key : element.getAttributeNames()) {
			String currentValue = this.getAttribute(key);
			String newValue = element.getAttribute(key);

			if (currentValue == null || !newValue.equals(currentValue)) {
				this.setAttribute(key, newValue);
			}
		}

		for (String key : new LinkedList<String>(this.getAttributeNames())) {
			String currentValue = element.getAttribute(key);

			if (currentValue == null) {
				this.removeAttribute(key);
			}
		}
		
		for(EventListener removedListener : new LinkedList<EventListener>(this.listeners)) {
			boolean found = false;
			
			for(EventListener addedListener : element.listeners) {
				if(removedListener.getClass().getSimpleName().equals(addedListener.getClass().getSimpleName())) {
					found = true;
					break;
				}
			}

			if(!found) {
				this.unregisterEventListener(removedListener);
			}
		}
		

		for(EventListener addedListener : element.listeners) {
			boolean found = false;
			
			for(EventListener removedListener : this.listeners) {
				if(removedListener.getClass().getSimpleName().equals(addedListener.getClass().getSimpleName())) {
					found = true;
					break;
				}
			}

			if(!found) {
				this.registerEventListener(addedListener);
			}
		}
	}

	@Override
	public String getCode() {
		this.stringBuilder.setLength(0);
		this.stringBuilder.append("<");
		this.stringBuilder.append(getName());

		if (!attributes.isEmpty()) {
			for (String key : attributes.keySet()) {
				this.stringBuilder.append(" ");
				this.stringBuilder.append(key);
				this.stringBuilder.append("=\"");
				this.stringBuilder.append(attributes.get(key));
				this.stringBuilder.append("\"");
			}
		}
		if (!getChildNodes().isEmpty()) {
			this.stringBuilder.append(">");
			for (Node node : getChildNodes()) {
				this.stringBuilder.append(node.getCode());
			}
			this.stringBuilder.append("</");
			this.stringBuilder.append(getName());
			this.stringBuilder.append(">");
		} else {
			this.stringBuilder.append("/>");
		}
		return this.stringBuilder.toString();
	}

	public String getComponentType() {
		return this.getAttribute("j:component");
	}

	public Element setComponentType(String value) {
		this.setAttribute("j:component", value);

		return this;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	@Override
	public boolean isWatch() {
		if (attributes.containsKey("j:component") || attributes.containsKey("j:id")) {
			setWatch(true);
		}

		return super.isWatch();
	}

	@Override
	public String getHash() {
		return name;
	}
}
