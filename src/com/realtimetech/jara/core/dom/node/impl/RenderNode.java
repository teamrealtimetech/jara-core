package com.realtimetech.jara.core.dom.node.impl;

import com.realtimetech.jara.core.dom.Document;
import com.realtimetech.jara.core.dom.node.Node;

public class RenderNode extends Document {
	private Node renderTargetNode;

	public RenderNode(Node renderTargetNode) {
		super(null);
		this.renderTargetNode = renderTargetNode;
	}

	@Override
	public void reflectionNode(Node node) {
	}

	@Override
	public String getCode() {
		return renderTargetNode.getCode();
	}

	@Override
	public String getHash() {
		return renderTargetNode.getHash();
	}

	public Node getRenderTargetNode() {
		return renderTargetNode;
	}
}
