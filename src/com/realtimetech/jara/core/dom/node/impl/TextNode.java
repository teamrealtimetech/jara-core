package com.realtimetech.jara.core.dom.node.impl;

import com.realtimetech.jara.core.dom.changes.ChangeType;
import com.realtimetech.jara.core.dom.node.Node;

public class TextNode extends Node {
	private String text;

	public TextNode(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public TextNode setText(String text) {
		this.text = text;
		this.getDocument().writeChange(ChangeType.MODIFY_TEXT, this.getIndex(), this.text);
		
		return this;
	}

	@Override
	public void reflectionNode(Node node) {
		if (node instanceof TextNode) {
			TextNode oldNode = (TextNode) node;
			if (!this.text.equals(oldNode.getText())) {
				this.setText(oldNode.text);
			}
		}
	}

	@Override
	public String getHash() {
		return "TEXT";
	}

	@Override
	public String getCode() {
		return text;
	}
}
