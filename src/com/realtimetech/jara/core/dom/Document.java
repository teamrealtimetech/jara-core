package com.realtimetech.jara.core.dom;

import java.util.HashMap;

import com.realtimetech.jara.core.application.component.Component;
import com.realtimetech.jara.core.application.component.ComponentTree;
import com.realtimetech.jara.core.application.component.listener.impl.ValueChangeEventListener;
import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.dom.changes.ChangeType;
import com.realtimetech.jara.core.dom.changes.Changes;
import com.realtimetech.jara.core.dom.node.Node;
import com.realtimetech.jara.core.dom.node.impl.Element;

public class Document extends Element {
	private boolean working;

	private Page page;

	private Changes changes;

	private long globalEventId;
	
	private long globalIndex;

	private HashMap<Long, Node> elementByIndex;

	private HashMap<String, Element> elementByJId;

	public Document(Page page) {
		super("DOCUMENT");

		this.page = page;
		this.working = false;
		this.globalEventId = 0;
		this.globalIndex = 0;

		this.changes = new Changes();
		this.elementByIndex = new HashMap<Long, Node>();
		this.elementByJId = new HashMap<String, Element>();
	}

	@Override
	public String getName() {
		return "document";
	}

	public long getGlobalIndex() {
		return globalIndex;
	}

	public void registerElementWithJId(String jId, Element element) {
		this.elementByJId.put(jId, element);
	}

	public Element getElementByJId(String jId) {
		return this.elementByJId.get(jId);
	}

	public void executeJs(String script) {
		writeChange(true, ChangeType.EXECUTE_JS, script);
	}

	public void writeChange(ChangeType changeType, Object... objects) {
		writeChange(false, changeType, objects);
	}

	public void writeChange(boolean update, ChangeType changeType, Object... objects) {
		if (working) {
			if (changes != null) {
				changes.write(changeType, objects);

				if (update) {
					page.getSession().update();
				}
			}
		}
	}

	public void registerNode(Node node) {
		if (working && node.isWatch()) {
			if (node.getIndex() == -1) {
				node.setIndex(nextIndex());

				this.elementByIndex.put(node.getIndex(), node);

				if (node instanceof Element) {
					Element element = (Element) node;
					Component component = page.getSession().getApplication().buildComponent(this.page, element);
					if (component != null) {
						ComponentTree componentTree = page;
						Node parentNode = element.getParentNode();

						while (parentNode != this) {
							if (parentNode instanceof Element) {
								Element parentElement = (Element) parentNode;

								if (parentElement.getComponent() != null) {
									componentTree = parentElement.getComponent();
									break;
								}
							}

							parentNode = parentNode.getParentNode();
						}

						componentTree.addComponent(component);
					}

					String attribute = element.getAttribute("j:id");

					if (attribute != null) {
						this.registerElementWithJId(attribute, element);
					}

					element.registerEventListener(new ValueChangeEventListener() {
						@Override
						public void onChange(String value) {
							element.setValue(value);
						}
					});
				}
			}
		}
	}

	public Node getNodeByIndex(long index) {
		return this.elementByIndex.get(index);
	}

	public long nextEventId() {
		this.globalEventId += 1;

		return this.globalEventId;
	}
	
	private long nextIndex() {
		this.globalIndex += 1;

		return this.globalIndex;
	}

	public Changes getChanges() {
		return changes;
	}

	@Override
	public Document getDocument() {
		return this;
	}

	@Override
	public Node getParentNode() {
		if (!working) {
			return null;
		}

		return this;
	}

	@Override
	public boolean isWatch() {
		return false;
	}

	public boolean isWorking() {
		return working;
	}

	public void active() {
		this.working = true;

		this.updateTreeDown();
	}

	public Page getPage() {
		return page;
	}
}
