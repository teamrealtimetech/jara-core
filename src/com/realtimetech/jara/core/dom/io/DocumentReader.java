package com.realtimetech.jara.core.dom.io;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.xml.sax.SAXException;

import com.realtimetech.jara.core.application.Application;
import com.realtimetech.jara.core.application.page.Page;
import com.realtimetech.jara.core.dom.Document;
import com.realtimetech.jara.core.dom.node.Node;
import com.realtimetech.jara.core.dom.node.impl.Element;
import com.realtimetech.jara.core.dom.node.impl.TextNode;

public class DocumentReader {
	private Application application;

	public DocumentReader(Application application) {
		this.application = application;
	}

	public Application getApplication() {
		return application;
	}

	public Document read(Page page, String source) throws SAXException, IOException {
		org.jsoup.nodes.Document parsedDocument = Jsoup.parse(source);

		Document document = new Document(page);
		List<org.jsoup.nodes.Node> nodes = parsedDocument.childNodes();

		for (int index = 0; index < nodes.size(); index++) {
			recursiveNodeConverter(nodes.get(index), document);
		}

		return document;
	}

	private Node recursiveNodeConverter(org.jsoup.nodes.Node sourceNode, Element parentElement) {
		Node targetNode = null;

		if (sourceNode instanceof org.jsoup.nodes.Element) {
			org.jsoup.nodes.Element element = (org.jsoup.nodes.Element) sourceNode;

			targetNode = new Element(element.tagName());
		} else if (sourceNode instanceof org.jsoup.nodes.TextNode) {
			org.jsoup.nodes.TextNode textNode = (org.jsoup.nodes.TextNode) sourceNode;

			targetNode = new TextNode(textNode.text());
		} else if (sourceNode instanceof org.jsoup.nodes.DataNode) {
			org.jsoup.nodes.DataNode dataNode = (org.jsoup.nodes.DataNode) sourceNode;

			targetNode = new TextNode(dataNode.getWholeData());
		}

		if (targetNode != null) {
			parentElement.addChildNode(targetNode);
		}

		if (sourceNode.attributes() != null && targetNode instanceof Element) {
			Element tagNode = (Element) targetNode;
			Attributes attributeMap = sourceNode.attributes();

			for (Attribute attribute : attributeMap.asList()) {
				tagNode.setAttribute(attribute.getKey(), attribute.getValue());
			}
		}

		List<org.jsoup.nodes.Node> nodes = sourceNode.childNodes();
		for (int index = 0; index < nodes.size(); index++) {
			recursiveNodeConverter(nodes.get(index), targetNode instanceof Element ? (Element) targetNode : null);
		}

		return targetNode;
	}
}
