# [Jara](https://bitbucket.org/teamrealtimetech/jara-core/src/master)
---
**Jara**는 Back-end에서 View를 설계/관리할 수 있는 Java 기반 Web Application FrameWork입니다.

* **Server-Side-Rendering** : Jara는 서버단에서 View를 설계하고, 동적인 요소를 관리합니다. 이 덕분에 개발 측면에서 직관적인 코드 작성과 디버깅이 간편합니다.
* **DOM Tree Update 방식** : 모든 Node를 Update하는 것이 아닌, Virtual DOM을 이용해 Data가 변경된 Node만을 찾아 Update 합니다.
* **Component 단위 관리** : 독립적인 View를 Component 단위로 구현한 뒤, 여러 곳에서 호출하여 쉽게 사용할 수 있습니다.

## Installation
---
[Jara Release Note](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Jara%20Release%20Note)에서 Release를 다운받아 사용할 수 있습니다.

## Dependencies
---

* [jara-http](https://bitbucket.org/teamrealtimetech/jara-http/src/master/)
* [carbon](https://bitbucket.org/teamrealtimetech/carbon/src/master/)
* [jsoup](https://github.com/jhy/jsoup)

## Documentation
---
Jara 학습에 도움이 되는 자료와 튜토리얼들입니다.

자료 :

* [Jara Summary](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Jara%20Summary)
* [Jara Release Note](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Jara%20Release%20Note)

튜토리얼:

* Base Part :
	* [Jara Installation](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Jara%20Installation)
	* [Run Application](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Run%20Application)  
	* [Page Routing](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Page%20Routing)  
	* [Static Routing](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Static%20Routing)  
* View Part :
	* [Create Element](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Create%20Element)
	* [Create Component](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Create%20Component)
	* [Register Event](https://bitbucket.org/teamrealtimetech/jara-core/wiki/Register%20Event)

## License
---

Jara는 [Apache License 2.0](./LICENSE.txt) 라이센스를 이용합니다, 여러분의 적극적인 이슈, 기능 피드백을 기대합니다.

```
JeongHwan, Park
+821032735003
parkjeonghwan@realtimetech.co.kr
```
